import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.mapreduce.Job;

/**
 * main PageRank (with Jacobi) Hadoop job drivers
 * run and check for terminating condition
 * 
 * @author wt255
 */
public class JacobiJob {
	private static Log log = LogFactory.getLog(JacobiJob.class);
	
	private static String inputPath;
	private static String outputPath;
	private static boolean parsed = false;
	
    private static JobConf conf   = null;
    private static Job currentJob = null;
    private static int iteration  = 0;
    
    public static long   dummyPage  = 999999;
    public static long   pageCount  = 685230;
    public static double dampFactor = 0.85;
    public static double epsilon    = 0.001;
    
   
	/**
	 * @param  args command line parameter 
	 *         arg[0]-input dir 
	 * 		   arg[1]-output dir
	 *         arg[2]-special param: parsed  - the input is already parsed
	 *                               6       - work on test mode with 6 nodes and 2 blocks
	 *                               6parsed - both of above
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		inputPath = args[0];
		outputPath = args[1];
		
		if(args.length > 2) {
			if(args[2].indexOf(6) != -1) pageCount = 6; 
			if(args[2].indexOf("parsed") != -1) parsed = true; 
		}
		
		// parse input (default)
		if(!parsed) inputPath = ParseInput.runParseJob(inputPath);
		
		long blockIterationCount=1; 
		//Changed to <=6 from < 20 for random blocking as per requirement of the project 
        while(iteration <= 6 && blockIterationCount>0) {
        	iteration++;
        	System.out.println("");
			System.out.println("Starting iteration: " + iteration);
	        initJobConf();
        	currentJob = createNewJob();
        	currentJob.waitForCompletion(true);
	        
	        blockIterationCount = currentJob.getCounters().findCounter(JacobiCounter.TOTAL_BLOCK_ITERATIONS).getValue();
	        double errorSum = currentJob.getCounters().findCounter(JacobiCounter.TOTAL_ERROR_10_EXP_8).getValue()/100000000;
	        log.info("iteration=" + iteration + " JACOBI_COUNTER=" + blockIterationCount + " err=" + errorSum + 
	        		" threshold=" + (epsilon*pageCount));
        	System.out.println("");
        	if( errorSum < epsilon*pageCount ) {
        		log.info("Error is within threshold! Done.");
        		return;
        	}
        }
	}
	
	
	/**
	 * initialize job configuration
	 * @param inputDir
	 * @param outputDir
	 */
	public static void initJobConf(){
		conf = new JobConf(JacobiJob.class);
		
		// constants
		conf.set("dampfactor", String.valueOf(dampFactor));
		conf.set("pagecount", String.valueOf(pageCount));  	// fullgraph=685299
		conf.set("epsilon", String.valueOf(epsilon));
		conf.set("dummypage", String.valueOf(dummyPage));
		
        // configure mapper and reducer
        conf.setMapperClass(JacobiMapper.class);
        conf.setReducerClass(JacobiReducer.class);
        
        // configure key/value data types
        conf.setMapOutputKeyClass(LongWritable.class);
        conf.setMapOutputValueClass(Edge.class);
        conf.setOutputKeyClass(Edge.class);
        conf.setOutputValueClass(DoubleWritable.class);
        
        // hdfs file i/o
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
	}
	
	/**
	 * create a ready-to-run job for the next iteration
	 * @return Job to be executed next
	 * @throws IOException 
	 */
	public static Job createNewJob() throws IOException{
        if(iteration == 1) {
	        TextInputFormat.addInputPath(conf, new Path(inputPath));
        } else {
	        TextInputFormat.addInputPath(conf, new Path(outputPath + (iteration-1)));
        }
        
        // configuration should contain reference to your namenode
        FileSystem fs = FileSystem.get(new Configuration());
        // true stands for recursively deleting the folder you gave
        try{ fs.delete(new Path(outputPath + iteration), true);
        } catch(Exception e){}
	    
        TextOutputFormat.setOutputPath(conf, new Path(outputPath + iteration));
        
	    // Create a new Job
	    Job job = null;
		try {
			job = new Job(conf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    job.setJobName("Jacobi Iteration " + iteration);
	    return job;
	}
	
	
}
