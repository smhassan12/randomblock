/**
 * User-defined Hadoop Counter
 * @author wt255
 */
public enum JacobiCounter {
	TOTAL_BLOCK_ITERATIONS, // accumulated number of iterations from all reducers
	TOTAL_ERROR_10_EXP_8,   // accumulated error (mutiplied by 10^8)
	TOTAL_REJECTED			// accumulated number of edge rejected
}
